from django.contrib import admin
from .models import Genre, Movie
from django.core import serializers
from django.http import HttpResponse

def export_as_json(modeladmin, request, queryset):
    response = HttpResponse(content_type="application/json")
    serializers.serialize("json", queryset, stream=response)
    return response

class GenreAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')

class MovieAdmin(admin.ModelAdmin):
    actions = ['delete_selected', 'export_as_json']
    exclude = ("date_created", )
    list_display = ('title', 'number_in_stock', 'daily_rate')

admin.site.register(Genre, GenreAdmin)
admin.site.register(Movie, MovieAdmin)
admin.site.add_action(export_as_json, 'Export as JSON')
